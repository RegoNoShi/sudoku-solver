package com.regonoshi.sudoku.exceptions;

public class InvalidSudokuException extends Exception {

	private static final long serialVersionUID = 2638974597451566570L;

	public InvalidSudokuException() {
		super("Invalid Sudoku grid!");
	}
	
}
