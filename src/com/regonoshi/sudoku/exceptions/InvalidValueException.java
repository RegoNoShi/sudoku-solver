package com.regonoshi.sudoku.exceptions;

import com.regonoshi.sudoku.Cell;

public class InvalidValueException extends Exception {

	private static final long serialVersionUID = 5923700214201538068L;

	public InvalidValueException(byte value) {
		super("Invalid value " + value + "! "
				+ "Valid values are in range [" + (Cell.emptyValue + 1) + "-" + Cell.maxValue + "]");
	}
	
}
