package com.regonoshi.sudoku.exceptions;

public class SudokuAlreadySolvedException extends Exception {

	private static final long serialVersionUID = 2638974597451566570L;

	public SudokuAlreadySolvedException() {
		super("Sudoku already solved!");
	}
	
}
