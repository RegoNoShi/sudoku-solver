package com.regonoshi.sudoku;

import java.util.HashSet;

public class Cell {

	public static final byte maxValue = 9;
	public static final byte emptyValue = 0;
	private byte value;
	private boolean fixed;	
	
	private Cell(byte value) {
		this.value = value;
		this.fixed = value != emptyValue;
	}
	
	public Cell(Cell c) {
		this.value = c.value;
		this.fixed = c.fixed;
	}
	
	public static Cell emptyCell() {
		return new Cell(emptyValue);
	}
	
	public static Cell initialCell(byte value) {
		if(value <= emptyValue || value > maxValue)
			return null;
		else
			return new Cell(value);
	}

	public static HashSet<Byte> getAllValidValues() { 
		HashSet<Byte> allValidValues = new HashSet<Byte>();
		for(byte b = emptyValue + 1; b <= maxValue; b++)
			allValidValues.add(b);
		return allValidValues;
	}
	
	public static boolean isValidValue(byte value) {
		return value > emptyValue && value <= maxValue;
	}
	
	public byte getValue() {
		return value;
	}

	public boolean setValue(byte value) {
		if(this.fixed)
			return false;
		else if(!isValidValue(value))
			return false;
		this.value = value;
		return true;
	}
	
	public boolean resetValue() {
		if(this.fixed)
			return false;
		this.value = emptyValue;
		return true;
	}

	public boolean isFixed() {
		return fixed;
	}

	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	public boolean isEmpty() {
		return value == emptyValue;
	}
	
	public String toString() {
		return isEmpty() ? " " : String.valueOf(value);
	}
	
}
