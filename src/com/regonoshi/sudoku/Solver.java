package com.regonoshi.sudoku;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import com.regonoshi.sudoku.exceptions.InvalidSudokuException;
import com.regonoshi.sudoku.exceptions.InvalidValueException;
import com.regonoshi.sudoku.exceptions.SudokuAlreadySolvedException;

public class Solver {

	private Grid grid;
	private HashSet<Byte> [] missingRowsValues;
	private HashSet<Byte> [] missingColsValues;
	private HashSet<Byte> [][] missingBlocksValues;
	private ArrayList<Insertable> insertables;
	private BigInteger count;
	private int next;
	private static boolean solved;
	private static double startTime;
	private static int tryCount = 0;
	
	public static void solve(Grid g, GridGUI gui) throws SudokuAlreadySolvedException {
		startTime = System.nanoTime();
		solved = false;
		tryCount = 0;
		solvePr(g, gui);
	}
	
	private static void solvePr(Grid g, GridGUI gui) throws SudokuAlreadySolvedException {
		if(g != null && g.isSolved()) {
			solved = true;
			double elapsed = (System.nanoTime() - startTime) / 1000000000;
			DecimalFormat df = new DecimalFormat("#.00"); 
			gui.updateLabel("Solved! " + df.format(elapsed) + " s | " + tryCount + " try");
			gui.update(g);
			return;
		}
		tryCount++;
		if(solved || g == null || !g.isValid())
			return;
		gui.update(g);
		Solver sln = new Solver(g);
		while(!solved && sln.hasNext())
			solvePr(sln.next(), gui);
	}
	
	
	public Solver(Grid grid) throws SudokuAlreadySolvedException {
		if(grid.isSolved())
			throw new SudokuAlreadySolvedException();
		this.grid = grid;
		solved = false;
		HashSet<Byte> allValidValues = Cell.getAllValidValues();
		missingRowsValues = new HashSet[Grid.rows];
		for(int r = 0; r < Grid.rows; r++)
			missingRowsValues[r] = (HashSet<Byte>)allValidValues.clone();
		missingColsValues = new HashSet[Grid.cols];
		for(int c = 0; c < Grid.cols; c++)
			missingColsValues[c] = (HashSet<Byte>)allValidValues.clone();
		missingBlocksValues = new HashSet[Grid.rows / Grid.blockRows][Grid.cols / Grid.blockCols];
		for(int br = 0; br < (Grid.rows / Grid.blockRows); br++)
			for(int bc = 0; bc < (Grid.cols / Grid.blockCols); bc++)
				missingBlocksValues[br][bc] = (HashSet<Byte>)allValidValues.clone();
		for(int r = 0; r < Grid.rows; r++) {
			for(int c = 0; c < Grid.cols; c++) {
				if(!grid.isFixed(r, c)) {
					byte v = grid.getValue(r, c);
					missingRowsValues[r].remove(v);
					missingColsValues[c].remove(v);
					missingBlocksValues[r / Grid.blockRows][c / Grid.blockCols].remove(v);
				}
			}
		}
		updateInsertables();
	}
	
	public void printMissing() {
		System.out.println("MISSING:");
		for(int r = 0; r < Grid.rows; r++) {
			if(!missingRowsValues[r].isEmpty()) {
				System.out.print("R " + (r + 1) + ":\t\t");
				for(byte b : missingRowsValues[r]) {
					System.out.print(b + "\t");
				}
				System.out.println();
			}
		}
		for(int c = 0; c < Grid.cols; c++) {
			if(!missingColsValues[c].isEmpty()) {
				System.out.print("C " + (c + 1) + ":\t\t");
				for(byte b : missingColsValues[c]) {
					System.out.print(b + "\t");
				}
				System.out.println();
			}
		}
		for(int r = 0; r < (Grid.rows / Grid.blockRows); r++) {
			for(int c = 0; c < (Grid.cols / Grid.blockCols); c++) {
				if(!missingBlocksValues[r][c].isEmpty()) {
					System.out.print("B " + (r + 1) + "-" + (c + 1) + ":\t\t");
					for(byte b : missingBlocksValues[r][c]) {
						System.out.print(b + "\t");
					}
					System.out.println();
				}
			}
		}
		System.out.println();
	}
	
	private void updateInsertables() {
		insertables = new ArrayList<Insertable>();
		count = BigInteger.ONE;
		next = 0;
		if(grid.isSolved())
			return;
		for(int r = 0; r < Grid.rows; r++) {
			for(int c = 0; c < Grid.cols; c++) {
				if(grid.isEmpty(r, c)) {
					HashSet<Byte> validValues = new HashSet<Byte>(missingRowsValues[r]);
					validValues.retainAll(missingColsValues[c]);
					validValues.retainAll(missingBlocksValues[r / Grid.blockRows][c / Grid.blockCols]);
					insertables.add(new Insertable(r, c, validValues));
					count = count.multiply(BigInteger.valueOf(validValues.size()));
				}
			}
		}
		for(Insertable i : insertables) {
			HashSet<Byte> validValues = (HashSet<Byte>)i.getValidValues().clone();
			switch(i.getR() % Grid.blockRows) {
			case 0:
				validValues.removeAll(missingRowsValues[i.getR() + 1]);
				validValues.removeAll(missingRowsValues[i.getR() + 2]);
				break;
			case 1:
				validValues.removeAll(missingRowsValues[i.getR() - 1]);
				validValues.removeAll(missingRowsValues[i.getR() + 1]);
				break;
			case 2:
				validValues.removeAll(missingRowsValues[i.getR() - 1]);
				validValues.removeAll(missingRowsValues[i.getR() - 2]);
			}
			switch(i.getC() % Grid.blockCols) {
			case 0:
				validValues.removeAll(missingColsValues[i.getC() + 1]);
				validValues.removeAll(missingColsValues[i.getC() + 2]);
				break;
			case 1:
				validValues.removeAll(missingColsValues[i.getC() - 1]);
				validValues.removeAll(missingColsValues[i.getC() + 1]);
				break;
			case 2:
				validValues.removeAll(missingColsValues[i.getC() - 1]);
				validValues.removeAll(missingColsValues[i.getC() - 2]);
			}
			if(!validValues.isEmpty()) {
				if(validValues.size() == 1)
					i.setValidValues(validValues);
			}
		}
		Collections.sort(insertables);
	}
	
	public void printInsertable() {
		System.out.println("INSERTABLE:");
		for(Insertable i : insertables) {
			System.out.println(i);
		}
		System.out.println();
	}
	
	public boolean hasInsertables() {
		return !insertables.isEmpty();
	}
	
	public boolean hasNext() {
		Insertable i = insertables.get(0);
		return next < i.getValidValuesSize();
	}
	
	public Grid next() {
		Grid tmp;
		try {
			tmp = new Grid(grid);
		} catch (InvalidValueException e) {
			e.printStackTrace();
			return null;
		} catch(InvalidSudokuException e) {
			e.printStackTrace();
			return null;
		}
		int row, col;
		byte value;
		Insertable i = insertables.get(0);
		row = i.getR();
		col = i.getC();
		value = i.getValidValue(next);
		next++;
		if(tmp.insert(row, col, value))
			return tmp;
		else
			return null;
	}
	
	public int getInsertablesSize() {
		return insertables.size();
	}
	
	public Insertable getInsertable(int pos) {
		if(pos >= 0 && pos < insertables.size())
			return insertables.get(pos);
		return null;
	}
	
	public BigInteger getCombinations() {
		return count;
	}
	
}
