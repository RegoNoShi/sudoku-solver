package com.regonoshi.sudoku;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.border.MatteBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import com.regonoshi.sudoku.exceptions.InvalidSudokuException;
import com.regonoshi.sudoku.exceptions.InvalidValueException;
import com.regonoshi.sudoku.exceptions.SudokuAlreadySolvedException;

public class GridGUI extends JFrame implements ActionListener, ComponentListener {

	private static final long serialVersionUID = 5806763478910250991L;
	private JTextField [][] grid;
	private JButton solveButton, easyButton, mediumButton, hardButton;
	private JLabel alert;
	private Grid g;
	private long delay = 0;
	private static MatteBorder top = BorderFactory.createMatteBorder(5, 1, 1, 1, Color.BLACK);
	private static MatteBorder left = BorderFactory.createMatteBorder(1, 5, 1, 1, Color.BLACK);
	private static MatteBorder topLeft = BorderFactory.createMatteBorder(5, 5, 1, 1, Color.BLACK);
	private static MatteBorder none = BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK);
	private static Font bold = new Font("Courier", Font.BOLD, 20);
	private static Font smallBold = new Font("Courier", Font.BOLD, 14);
	
	private class JTextFieldLimit extends PlainDocument {
		
		private static final long serialVersionUID = 4345290225632464052L;
		
		public void insertString(int offset, String  str, AttributeSet attr ) throws BadLocationException {
			if(str == null)
				return;
			try {
				if(this.getLength() == 0 && Cell.isValidValue(Byte.parseByte(str)))
					super.insertString(offset, str, attr);
			} catch(NumberFormatException e) {
				super.insertString(offset, "", attr);
			}
		}
		
	}
	
	public GridGUI() throws Exception {
		this.getContentPane().setLayout(new GridBagLayout());
		this.setSize(new Dimension(500, 500));
		this.setMinimumSize(new Dimension(500, 500));
		grid = new JTextField[Grid.rows][Grid.cols];
		int size = Math.min(this.getHeight() / 11, this.getWidth() / 11);
		Dimension dim = new Dimension(size, size);
		GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.fill = GridBagConstraints.NONE;
		for(int r = 0; r < 9; r++) {
			for(int c = 0; c < 9; c++) {
				grid[r][c] = new JTextField();
				grid[r][c].setHorizontalAlignment(SwingConstants.CENTER);
				grid[r][c].setFont(bold);
				grid[r][c].setForeground(Color.RED);
				grid[r][c].setMaximumSize(dim);
				grid[r][c].setMinimumSize(dim);
				grid[r][c].setPreferredSize(dim);
				grid[r][c].setDocument(new JTextFieldLimit());
				if((r % 3) == 0 && (c % 3) == 0 && r > 0 && c > 0)
					grid[r][c].setBorder(topLeft);
				else if((r % 3) == 0 && r > 0)
					grid[r][c].setBorder(top);
				else if((c % 3) == 0 && c > 0)
					grid[r][c].setBorder(left);
				else
					grid[r][c].setBorder(none);
				gbc.gridy = r;
				gbc.gridx = c;
				this.add(grid[r][c], gbc);
			}
		}
	    solveButton = new JButton("Solve");
	    solveButton.addActionListener(this);
	    easyButton = new JButton("Set Easy");
	    easyButton.setActionCommand("EASY");
	    easyButton.addActionListener(this);
	    mediumButton = new JButton("Set Medium");
	    mediumButton.setActionCommand("MEDIUM");
	    mediumButton.addActionListener(this);
	    hardButton = new JButton("Set Hard");
	    hardButton.setActionCommand("HARD");
	    hardButton.addActionListener(this);
	    gbc = new GridBagConstraints();
	    gbc.gridy = 9;
		gbc.gridx = 0;
		gbc.gridwidth = 3;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		size = Math.min(this.getHeight() / 100, this.getWidth() / 100);
		gbc.insets = new Insets(size, size, size, size);
		this.getContentPane().add(easyButton, gbc);
		gbc.gridx = 3;
		this.getContentPane().add(mediumButton, gbc);
		gbc.gridx = 6;
		this.getContentPane().add(hardButton, gbc);
		gbc.gridy = 10;
		gbc.gridx = 0;
		this.getContentPane().add(solveButton, gbc);
		gbc.gridy = 10;
		gbc.gridx = 3;
		gbc.gridwidth = 6;
		alert = new JLabel();
		alert.setForeground(Color.RED);
		alert.setFont(smallBold);
		alert.setHorizontalAlignment(SwingConstants.CENTER);
		alert.setVisible(false);
		this.getContentPane().add(alert, gbc);
		this.getContentPane().addComponentListener(this);
		this.setTitle("Sudoku Solver");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	public void setGrid(String type) {
		try {
			switch(type) {
			case "EASY":
				g = new Grid(Grid.EASY);
				break;
			case "MEDIUM":
				g = new Grid(Grid.MEDIUM);
				break;
			case "HARD":
				g = new Grid(Grid.HARD);
				break;
			default:
				g = new Grid(Grid.HARD);
			}
		} catch (InvalidValueException e) {
			//e.printStackTrace();
			alert.setText("Invalid Value in Grid!!!");
			alert.setVisible(true);
			solveButton.setEnabled(true);
			easyButton.setEnabled(true);
			mediumButton.setEnabled(true);
			hardButton.setEnabled(true);
			return;
		} catch (InvalidSudokuException e) {
			//e.printStackTrace();
			alert.setText("Invalid Sudoku!!!");
			alert.setVisible(true);
			solveButton.setEnabled(true);
			easyButton.setEnabled(true);
			mediumButton.setEnabled(true);
			hardButton.setEnabled(true);
			return;
		}
		for(int r = 0; r < Grid.rows; r++) {
			for(int c = 0; c < Grid.cols; c++) {
				byte v = g.getValue(r, c);
				if(v == Cell.emptyValue) {
					grid[r][c].setText("");
					grid[r][c].setForeground(Color.BLUE);
					grid[r][c].setEditable(true);
				}
				else {
					grid[r][c].setText(String.valueOf(v));
					grid[r][c].setForeground(Color.RED);
					grid[r][c].setEditable(false);
				}
			}
		}
		update(g);
	}
	
	private void getGrid() {
		byte [][] init = new byte[Grid.rows][Grid.cols];
		for(int r = 0; r < Grid.rows; r++) {
			for(int c = 0; c < Grid.cols; c++) {
				grid[r][c].setEditable(false);
				String tmp = grid[r][c].getText();
				if(tmp == null || tmp.isEmpty())
					init[r][c] = 0;
				else
					init[r][c] = Byte.parseByte(tmp);
			}
		}
		try {
			g = new Grid(init);
		} catch (InvalidValueException e) {
			//e.printStackTrace();
			alert.setText("Invalid Value in Grid!!!");
			alert.setVisible(true);
			solveButton.setEnabled(true);
			easyButton.setEnabled(true);
			mediumButton.setEnabled(true);
			hardButton.setEnabled(true);
			return;
		} catch (InvalidSudokuException e) {
			//e.printStackTrace();
			alert.setText("Invalid Sudoku!!!");
			alert.setVisible(true);
			solveButton.setEnabled(true);
			easyButton.setEnabled(true);
			mediumButton.setEnabled(true);
			hardButton.setEnabled(true);
			return;
		}
		update(g);
	}

	public void update(Grid g) {
		for(int r = 0; r < 9; r++) {
			for(int c = 0; c < 9; c++) {
				if(g.isFixed(r, c)) {
					grid[r][c].setEditable(false);
					grid[r][c].setForeground(Color.RED);
				}
				else {
					grid[r][c].setEditable(true);
					grid[r][c].setForeground(Color.BLUE);
				}
				if(!g.isEmpty(r, c))
					grid[r][c].setText(String.valueOf(g.getValue(r, c)));
				else
					grid[r][c].setText("");
			}
		}
		this.invalidate();
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void updateLabel(String label) {
		alert.setVisible(true);
		alert.setText(label);
		this.invalidate();
	}
	
	@Override
	public void componentHidden(ComponentEvent arg0) {
	}

	@Override
	public void componentMoved(ComponentEvent arg0) {
	}

	@Override
	public void componentResized(ComponentEvent arg0) {
		Component comp = arg0.getComponent();
        int newSize = Math.min(comp.getHeight() / 11, comp.getWidth() / 11);
        Dimension dim = new Dimension(newSize, newSize);
        for(int r = 0; r < 9; r++) {
			for(int c = 0; c < 9; c++) {
				grid[r][c].setMaximumSize(dim);
				grid[r][c].setMinimumSize(dim);
				grid[r][c].setPreferredSize(dim);
			}
        }
        comp.invalidate();
	}

	@Override
	public void componentShown(ComponentEvent arg0) {
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		alert.setVisible(false);
		switch(arg0.getActionCommand()) {
		case "Solve":
			solveButton.setEnabled(false);
			easyButton.setEnabled(false);
			mediumButton.setEnabled(false);
			hardButton.setEnabled(false);
			SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
		        @Override
		        public Void doInBackground() {
		        	getGrid();
					try {
						Solver.solve(g, GridGUI.this);
					} catch (SudokuAlreadySolvedException e) {
						//e.printStackTrace();
						alert.setText("Sudoku already solved!!!");
						alert.setVisible(true);
						solveButton.setEnabled(true);
						easyButton.setEnabled(true);
						mediumButton.setEnabled(true);
						hardButton.setEnabled(true);
					}
		        	return null;
		        }
		        @Override
		        protected void done() {
		        }
		    };
		    worker.execute();
		    break;
		default:
			this.setGrid(arg0.getActionCommand());
		}
		this.invalidate();
	}
	
}
