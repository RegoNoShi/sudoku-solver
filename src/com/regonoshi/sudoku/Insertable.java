package com.regonoshi.sudoku;

import java.util.HashSet;

public class Insertable implements Comparable<Insertable> {
	
	private int r;
	private int c;
	private HashSet<Byte> validValues;
	
	public Insertable(int r, int c, HashSet<Byte> validValues) {
		super();
		this.r = r;
		this.c = c;
		this.validValues = validValues;
	}

	@Override
	public int compareTo(Insertable that) {
		return this.validValues.size() == that.validValues.size() ? 0 : 
			(this.validValues.size() > that.validValues.size() ? 1 : -1);
	}

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}

	public int getC() {
		return c;
	}

	public void setC(int c) {
		this.c = c;
	}

	public HashSet<Byte> getValidValues() {
		return validValues;
	}

	public void setValidValues(HashSet<Byte> validValues) {
		this.validValues = validValues;
	}
	
	public boolean isValidValue(byte v) {
		return validValues.contains(v);
	}
	
	public int getValidValuesSize() {
		return validValues.size();
	}
	
	public byte getValidValue(int pos) {
		return validValues.toArray(new Byte[0])[pos];
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append((r + 1) + " - " + (c + 1) + "\t\t");
		for(Byte value : validValues)
			sb.append(value + "\t");
		return sb.toString();
	}

}
