package com.regonoshi.sudoku;

import com.regonoshi.sudoku.exceptions.InvalidSudokuException;
import com.regonoshi.sudoku.exceptions.InvalidValueException;

public class Grid {
	
	public static final int rows = 9, cols = 9;
	public static final int blockRows = 3, blockCols = 3;
	private Cell [][] grid;
	private int validValuesCount, fixedValuesCount;
	private String line;
	
	public static byte [][] EASY = {
			{7, 0, 5, 0, 1, 8, 0, 0, 2},
			{4, 9, 0, 0, 0, 0, 3, 0, 0},
			{0, 2, 3, 6, 0, 0, 0, 0, 0},
			{1, 0, 6, 0, 2, 0, 0, 0, 0},
			{5, 4, 0, 0, 3, 0, 0, 2, 9},
			{0, 0, 0, 0, 8, 0, 6, 0, 3},
			{0, 0, 0, 0, 0, 1, 5, 3, 0},
			{0, 0, 4, 0, 0, 0, 0, 7, 1},
			{3, 0, 0, 5, 4, 0, 2, 0, 8}
	};
	
	public static byte [][] MEDIUM = {
			{5, 3, 0, 0, 7, 0, 0, 0, 0},
			{6, 0, 0, 1, 9, 5, 0, 0, 0},
			{0, 9, 8, 0, 0, 0, 0, 6, 0},
			{8, 0, 0, 0, 6, 0, 0, 0, 3},
			{4, 0, 0, 8, 0, 3, 0, 0, 1},
			{7, 0, 0, 0, 2, 0, 0, 0, 6},
			{0, 6, 0, 0, 0, 0, 2, 8, 0},
			{0, 0, 0, 4, 1, 9, 0, 0, 5},
			{0, 0, 0, 0, 8, 0, 0, 7, 9}
	};
	public static byte [][] HARD = {
			{8, 0, 0, 6, 0, 1, 3, 0, 0},
			{0, 0, 0, 0, 3, 0, 7, 0, 9},
			{0, 0, 6, 0, 0, 9, 0, 0, 0},
			{0, 0, 0, 0, 0, 8, 0, 4, 2},
			{0, 0, 2, 0, 0, 0, 8, 0, 0},
			{7, 6, 0, 1, 0, 0, 0, 0, 0},
			{0, 0, 0, 3, 0, 0, 2, 0, 0},
			{5, 0, 1, 0, 8, 0, 0, 0, 0},
			{0, 0, 9, 4, 0, 5, 0, 0, 8}
	};
	
	public Grid(byte [][] init) throws InvalidValueException, InvalidSudokuException {
		initialize(init);
	}
	
	public Grid(Grid g) throws InvalidValueException, InvalidSudokuException {
		this.validValuesCount = g.validValuesCount;
		this.fixedValuesCount = g.fixedValuesCount;
		this.line = g.line;
		grid = new Cell[rows][cols];
		for(int r = 0; r < rows; r++)
			for(int c = 0; c < cols; c++)
				grid[r][c] = new Cell(g.grid[r][c]);
		if(!isValid())
			throw new InvalidSudokuException();
	}
	
	private void initialize(byte [][] init) throws InvalidValueException, InvalidSudokuException {
		validValuesCount = 0;
		fixedValuesCount = 0;
		grid = new Cell[rows][cols];
		for(int r = 0; r < rows; r++) {
			for(int c = 0; c < cols; c++) {
				if(init[r][c] == Cell.emptyValue)
					grid[r][c] = Cell.emptyCell();
				else if(!Cell.isValidValue(init[r][c]))
					throw new InvalidValueException(init[r][c]);
				else {
					grid[r][c] = Cell.initialCell(init[r][c]);
					fixedValuesCount++;
					validValuesCount++;
				}
			}
		}
		line = "";
		for(int i = 0; i < ((2 * (cols + (cols / blockCols))) + 1); i++)
			line += "-";
		if(!isValid())
			throw new InvalidSudokuException();
	}
	
	public boolean isSolved() {
		return validValuesCount == (rows * cols);
	}
	
	public boolean isValid() {
		for(int r = 0; r < rows; r++) {
			boolean [] found = new boolean[Cell.maxValue];
			for(int c = 0; c < cols; c++) {
				if(grid[r][c].getValue() != 0) {
					if(found[grid[r][c].getValue() - 1])
						return false;
					else
						found[grid[r][c].getValue() - 1] = true;
				}
			}
		}
		for(int c = 0; c < cols; c++) {
			boolean [] found = new boolean[Cell.maxValue];
			for(int r = 0; r < rows; r++) {
				if(grid[r][c].getValue() != 0) {
					if(found[grid[r][c].getValue() - 1])
						return false;
					else
						found[grid[r][c].getValue() - 1] = true;
				}
			}
		}
		for(int r = 0; r < (rows / blockRows); r++) {
			for(int c = 0; c < (cols / blockCols); c++) {
				boolean [] found = new boolean[Cell.maxValue];
				for(int br = r * blockRows; br < (r + 1) * blockRows; br++) {
					for(int bc = c * blockCols; bc < (c + 1) * blockCols; bc++) {
						if(grid[br][bc].getValue() != 0) {
							if(found[grid[br][bc].getValue() - 1])
								return false;
							else
								found[grid[br][bc].getValue() - 1] = true;
						}
					}
				}
			}
		}
		return true;
	}
	
	public void resetAll() {
		for(int r = 0; r < rows; r++)
			for(int c = 0; c < cols; c++)
				if(!grid[r][c].isFixed())
					grid[r][c].resetValue();
		validValuesCount = fixedValuesCount;
	}
	
	public boolean remove(int row, int col) {
		if(!grid[row][col].isFixed()) {
			grid[row][col].resetValue();
			validValuesCount--;
			return true;
		}
		return false;
	}
	
	public boolean insert(int row, int col, byte value) {
		if(row < 0 || row >= rows)
			return false;
		if(col < 0 || col >= cols)
			return false;
		if(grid[row][col].isFixed())
			return false;
		if(!Cell.isValidValue(value))
			return false;
		if(!grid[row][col].isEmpty())
			return false;
		byte tmp = grid[row][col].getValue();
		grid[row][col].setValue(value);
		// TODO: Optimize with check only on row, col and block modified
		if(isValid()) {
			validValuesCount++;
			return true;
		}
		else {
			grid[row][col].setValue(tmp);
			return false;
		}
	}
	
	public void printGrid() {
		System.out.println("GRID:");
		for(int r = 0; r < rows; r++) {
			if((r % 3) == 0)
				System.out.println(line + "\t\t" + line);
			for(int c = 0; c < cols; c++) {
				if((c % 3) == 0)
					System.out.print("| ");
				System.out.print(grid[r][c].toString() + " ");
			}
			System.out.print("|\t\t");
			for(int c = 0; c < cols; c++) {
				if((c % 3) == 0)
					System.out.print("| ");
				System.out.print((grid[r][c].isFixed() ? "X" : " ") + " ");
			}
			System.out.println("|");
		}
		System.out.println(line + "\t\t" + line + "\n");
		System.out.println("\t\tFixed Values:\t\t" + fixedValuesCount);
		System.out.println("\t\tValid Values:\t\t" + validValuesCount);
		System.out.println("\t\tSolved? \t\t" + isSolved());
	}

	public byte getValue(int row, int col) {
		return grid[row][col].getValue();
	}
	
	public boolean isFixed(int row, int col) {
		return grid[row][col].isFixed();
	}
	
	public boolean isEmpty(int row, int col) {
		return grid[row][col].isEmpty();
	}
	
}
